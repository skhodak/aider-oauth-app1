from flask import Flask, redirect, url_for, session, request
import os
from functools import wraps
from authlib.integrations.flask_client import OAuth
from dotenv import load_dotenv

load_dotenv()
app = Flask(__name__)
app.secret_key = os.urandom(24)
app.config['GOOGLE_CLIENT_ID'] = os.getenv("GOOGLE_ID")
app.config['GOOGLE_CLIENT_SECRET'] = os.environ.get("GOOGLE_SECRET")

oauth = OAuth(app)
google = oauth.register(
    name='google',
    server_metadata_url='https://accounts.google.com/.well-known/openid-configuration',
    client_kwargs={'scope': 'openid email profile'}
)

def login_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if 'google_token' not in session:
            return redirect(url_for('login'))
        return f(*args, **kwargs)
    return decorated_function


@app.route('/')
@login_required
def index():
    resp = google.get('userinfo')
    me = resp.json()
    return f'Welcome, {me.get("email")}!' 


@app.route('/login')
def login():
    redirect_uri = url_for('authorized', _external=True)
    print(redirect_uri)
    return google.authorize_redirect(redirect_uri)


@app.route('/logout')
def logout():
    session.pop('google_token')
    return redirect(url_for('index'))


@app.route('/login/authorized')
def authorized():
    token = google.authorize_access_token()
    session['google_token'] = token
    return redirect(url_for('index'))


if __name__ == '__main__':
    app.run(debug=True)
